package at.bxm.tstack.storage;

import static javax.persistence.FetchType.*;
import static javax.persistence.GenerationType.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "doc_documents")
public class DbDocument {

  @Id
  @GeneratedValue(strategy = AUTO)
  private Long id;
  @Column(nullable = false)
  private String title;
  @Column
  private String author;
  @Column(nullable = false)
  private LocalDate date;
  @Column
  private String path;
  @ElementCollection(fetch = EAGER)
  @CollectionTable(name = "doc_tags")
  private Set<String> tags = new HashSet<>();

  public DbDocument() {
  }

  public DbDocument(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Set<String> getTags() {
    return tags;
  }

  public void setTags(Set<String> tags) {
    this.tags = tags;
  }
}
