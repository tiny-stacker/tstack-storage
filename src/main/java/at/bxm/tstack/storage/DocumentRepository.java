package at.bxm.tstack.storage;

import org.springframework.data.repository.CrudRepository;

public interface DocumentRepository extends CrudRepository<DbDocument, Long> {
}
