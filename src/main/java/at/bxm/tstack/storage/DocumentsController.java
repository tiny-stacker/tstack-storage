package at.bxm.tstack.storage;

import static java.util.UUID.*;
import static java.util.stream.Collectors.*;
import static java.util.stream.StreamSupport.*;
import static org.slf4j.LoggerFactory.*;
import static org.springframework.http.ResponseEntity.*;

import at.bxm.tstack.storage.api.Document;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/documents")
public class DocumentsController {

  private final DocumentRepository documentRepository;
  private final Logger logger = getLogger(getClass());

  @Autowired
  public DocumentsController(DocumentRepository documentRepository) {
    this.documentRepository=documentRepository;
  }

  @GetMapping
  public List<Document> getAll() {
    return toApi(documentRepository.findAll());
  }

  @PostMapping
  public ResponseEntity create(@RequestBody Document document, UriComponentsBuilder urlBuilder) {
    if (document.getId() != null) {
      throw new IllegalArgumentException("ID must not be provided");
    }
    logger.debug("Saving new {}", document);
    DbDocument created = documentRepository.save(fromApi(document, new DbDocument()));
    return created(urlBuilder.path("/documents/{id}").buildAndExpand(created.getId()).toUri()).build();
  }

  @GetMapping("/{id}")
  public Document getById(@PathVariable("id") Long id) {
    return toApi(documentRepository.findById(id).orElseThrow(NotFoundException::new));
  }

  @PutMapping("/{id}")
  public ResponseEntity save(@PathVariable("id") Long id, @RequestBody Document document, UriComponentsBuilder urlBuilder) {
    Optional<DbDocument> existing = documentRepository.findById(id);
    if (existing.isPresent()) {
      logger.debug("Updating existing {} with ID {}", document, id);
      documentRepository.save(fromApi(document, existing.get()));
      return ok().build();
    } else {
      logger.debug("Saving new {} with ID {}", document, id);
      DbDocument created = documentRepository.save(fromApi(document, new DbDocument(id)));
      return created(urlBuilder.path("/documents/{id}").buildAndExpand(created.getId()).toUri()).build();
    }
  }

  private Document toApi(DbDocument source) {
    return new Document(source.getId(), source.getTitle(), source.getAuthor(), source.getDate(), source.getPath(), source.getTags());
  }

  private List<Document> toApi(Iterable<DbDocument> source) {
    return stream(source.spliterator(), false)
        .map(this::toApi)
        .collect(toList());
  }

  private DbDocument fromApi(Document source, DbDocument target) {
    target.setTitle(source.getTitle());
    target.setAuthor(source.getAuthor());
    target.setDate(source.getDate());
    target.setPath(source.getPath());
    target.setTags(source.getTags());
    return target;
  }
}
