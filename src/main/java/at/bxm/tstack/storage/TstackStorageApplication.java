package at.bxm.tstack.storage;

import static org.slf4j.LoggerFactory.*;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;

@SpringBootApplication
public class TstackStorageApplication {

  private final Logger logger = getLogger(getClass());
  private final BuildProperties buildProperties;

  @Autowired
  public TstackStorageApplication(BuildProperties buildProperties) {
    this.buildProperties = buildProperties;
  }

  public static void main(String[] args) {
    SpringApplication.run(TstackStorageApplication.class, args);
  }

  @PostConstruct
  private void init() {
    logger.info("{}:{} version {} built on {}", buildProperties.getGroup(),
        buildProperties.getArtifact(), buildProperties.getVersion(), buildProperties.getTime());
  }
}
