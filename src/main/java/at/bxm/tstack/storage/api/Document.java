package at.bxm.tstack.storage.api;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Document implements Serializable {

  private static final long serialVersionUID = -5130711930820064828L;
  private Long id;
  private String title;
  private String author;
  private LocalDate date;
  private String path;
  private Set<String> tags = new HashSet<>();

  public Document() {
  }

  public Document(Long id, String title, String author, LocalDate date, String path, Set<String> tags) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.date = date;
    this.path = path;
    this.tags = tags;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Set<String> getTags() {
    return tags;
  }

  public void setTags(Set<String> tags) {
    this.tags = tags;
  }
}
