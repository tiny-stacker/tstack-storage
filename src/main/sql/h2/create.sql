create sequence hibernate_sequence start with 1 increment by 1
create table doc_documents (id bigint not null, author varchar(255), date date not null, title varchar(255) not null, primary key (id))
create table doc_tags (document_id bigint not null, tags varchar(255))
alter table doc_tags add constraint FKeo77xufqgqg3ek3mo9pt8sl54 foreign key (document_id) references doc_documents
