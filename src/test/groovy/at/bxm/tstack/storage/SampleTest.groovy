package at.bxm.tstack.storage

import static org.hamcrest.Matchers.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

@SpringBootTest(classes = TstackStorageApplication)
@WebAppConfiguration
class SampleTest extends Specification {

  MockMvc mockMvc
  @Autowired
  WebApplicationContext webApplicationContext

  void setup() {
    mockMvc = webAppContextSetup(webApplicationContext).build()
  }

  void sample() {
    expect:
    mockMvc.perform(get("/actuator/info"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString('"artifact":"tstack-storage"')))
  }
}
